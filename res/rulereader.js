var fs = require('fs'),
	db = require('./db.js');

var files = ['africa','antarctica','asia','australasia','europe','northamerica','southamerica'];
var dic = {}, dC = 0, c = 0;

files.forEach(function(f){
	fs.readFile(__dirname+'/tzdata2013d/'+f, {encoding: 'utf8'}, function(err, data){
		if(err)
			return console.log(err);
		data = data.split('\n');
		data = data.filter(function(e,i){
			return e.indexOf('Rule') == 0;
		});

		/*
		data = data.map(function(e){
			e = e.split(/\s/).filter(function(e){ return e.length; });
			return 'SELECT insert_rule(\''+e[1]+'\', \''+e[2]+'\', \''+e[3]+'\', \''+e[5]+'\', \''+e[6]+'\', \''+e[7]+'\', \''+e[8]+'\', \''+e[9]+'\');';
		});

		/*
		data.forEach(function(d){
			db._query(d,null,function(err,data){
				if(err) console.log('ERR: ' + d + ' : ' + err);
				else console.log('SUCCESS: ' + data);
			});
		});
		*/

		data.forEach(function(e){
			e = e.split(/\s/).filter(function(e){ return e.length; });
			if(!dic[e[1]])
				dic[e[1]] = [];
			var rec = {
				from_year: e[2],
				to_year: e[3],
				month: e[5],
				day: e[6],
				time: e[7],
				diff: e[8],
				letter: (e[9] == '-' ? null : e[9])
			};
			++c;
			dic[e[1]].push(rec);
		});

		if(++dC == files.length){			
			fs.writeFile(__dirname+'/sql/rules.json', JSON.stringify(dic), function(err, data){
				console.log(err || c+' rule entries. OK.');
			});
		}
	});
});