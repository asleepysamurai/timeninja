var fs = require('fs'),
	db = require('./db.js');

var files = ['africa','antarctica','asia','australasia','europe','northamerica','southamerica','backward'];
var dic = {}, dC = 0, c = 0;

files.forEach(function(f){
	fs.readFile(__dirname+'/tzdata2013d/'+f, {encoding: 'utf8'}, function(err, data){
		if(err)
			return console.log(err);
		data = data.split('\n');
		data = data.filter(function(e,i){
			return e.indexOf('Link') == 0;
		});
		data = data.map(function(e){
			return e.split(/\s/).filter(function(e){ return e.length; });
		});

		/*
		data.forEach(function(d){
			db._query('INSERT INTO links (key, value) VALUES($1,$2)',[d[2],d[1]],function(err,data){
				if(err) console.log('ERR: ' + d + ' : ' + err);
				else console.log('SUCCESS: ' + data);
			});
		});
		*/

		data.forEach(function(d){
			dic[d[2]] = d[1];
			++c;
		});

		if(++dC == files.length){
			fs.writeFile(__dirname+'/sql/links.json', JSON.stringify(dic), function(err, data){
				console.log(err || c+' link entries. OK.');
			});
		}
	});
});