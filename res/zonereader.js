var fs = require('fs'),
	db = require('./db.js');

var files = ['africa','antarctica','asia','australasia','europe','northamerica','southamerica'];
var dic = {}, dC = 0, c = 0;

files.forEach(function(f){
	fs.readFile(__dirname+'/tzdata2013d/'+f, {encoding: 'utf8'}, function(err, data){
		if(err)
			return console.log(err);
		data = data.split('\n');
		data = data.filter(function(e,i){
			return ((e.replace(/^(\t)+/,''))[0] != '#') && (e.indexOf('Rule') != 0) && (e.indexOf('Link') != 0);
		});
		data = data.map(function(e,i){
			if(e.length > 0){
				e = e.split(/\s/).filter(function(e){ return e.length > 0});
				var a = [],b = [];
				if(e.indexOf('Zone') == 0){
					for(var i=0;i<5;++i) a.push(e[i]);
					for(var i=5;i<e.length;++i) b.push(e[i]);
				}
				else{				
					for(var i=0;i<3;++i) a.push(e[i]);
					for(var i=3;i<e.length;++i) b.push(e[i]);
				}
				a.push(b.join(' '));
				e = a.join('\t');
			}
			return e.replace(/^(\t)+/g,'"').replace(/(\t)+/g,'","').replace(/(\W)+#+.*/g,'');
		});

		var zones = [], zone = null;
		data.forEach(function(e){
			if(e.indexOf('Zone') == 0){
				if(zone)
					zones.push((zone.join('",')+'"}\');').replace(',""','').replace('"','\'').replace('"','\''));
				zone = [e.replace(/^Zone",/,'SELECT insert_zone(').replace('","','",\'{"')];
			}
			else if(e.length != 0)
				zone.push('"'+e);
		});
		zones.push((zone.join('",')+'"}\');').replace(',""','').replace('"','\'').replace('"','\''));

		zones.forEach(function(z){
			z = z.replace(/^(SELECT insert_zone\()/,'').replace(/{/,'[').replace(/}'\);/,']\'').replace(/'/g,'').split(',');
			var name = z[0];
			z.shift();
			z = JSON.parse(z.join(','));

			var record, last_date;
			dic[name] = [];
			z.forEach(function(e,i){
				var u = i%4;
				if(u==0){
					record = {};
					++c;
					record.offset = e;
				}
				else if(u==1)
					record.rule = e == '-' ? null : e;
				else if(u==2)
					record.shortcode = e == '-' || e == 'zzz' ? null : e;
				else if(u==3){
					record.to = e;
					record.from = last_date ? last_date : null;
					last_date = e;
					if(i!=0)
						dic[name].push(record);
				}
			});
			record.from = last_date ? last_date : null;
			record.to = null;
			dic[name].push(record);			
		});

		/*
		zones.forEach(function(d){
			db._query(d,null,function(err,data){
				if(err) console.log('ERR: ' + d + ' : ' + err);
				else console.log('SUCCESS: ' + data);
			});
		});
		*/

		if(++dC == files.length){
			fs.writeFile(__dirname+'/sql/zones.json', JSON.stringify(dic), function(err, data){
				console.log(err || c + ' zone entries. OK.');
			});
		}		
	});
});