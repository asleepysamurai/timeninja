var fs = require('fs'),
	extend = require('./extend.js');

//var daysPerMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
var daysPerWeek = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

var minDate = '0 Jan 1 0:00:00';
var maxDate = '9999 Dec 31 23:59:59';

var data = {
	rules: JSON.parse(fs.readFileSync(__dirname+'/sql/rules.json', {encoding: 'utf8'})),
	zones: JSON.parse(fs.readFileSync(__dirname+'/sql/zones.json', {encoding: 'utf8'}))
};

var monthsPerYear = function(idx){
	var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	if(typeof idx == 'number')
		return m[idx-1];
	else
		return m.indexOf(idx) + 1;
};

Object.prototype.forEach = function(fn){
	var obj = this;
	for(var i in obj){
		if(obj.hasOwnProperty(i))
			fn(obj[i],i,obj);
	}
};

var ruleSorter = function(a,b){
	return b.date - a.date;
};

var timeComponents = function(time){
	var type, sign = 1;
	if(isNaN(time[0])){
		sign = -1;
		time = time.substr(1);
	}
	if(isNaN(time[time.length-1]))
		type = time[time.length-1];

	time = time.split(/:/g).map(function(t){ return parseInt(t); });
	for(var i=time.length;i<3;++i)
		time[i] = 0;

	return {
		sign: sign,
		hour: time[0] ? time[0] : 0,
		minute: time[1] ? time[1] : 0,
		second: time[2] ? time[2] : 0,
		type: type
	};
};

var dateComponents = function(date){
	date = date.split(/\s/g);
	
	var time = date[3] ? date[3] : '00:00:00', type;	

	date = {
		year: date[0] ? parseInt(date[0]) : null,
		month: date[1] ? monthsPerYear(date[1]) : 1,
		day: date[2] ? date[2] : 1,
	};

	time = timeComponents(time);
	date.hour = time.hour;
	date.minute = time.minute;
	date.second = time.second;
	date.type = time.type;

	return date;
};

var offsetInSeconds = function(offset){
	offset = timeComponents(offset);
	return offset.sign * ((offset.hour * 60 * 60) + (offset.minute * 60) + offset.second);
};

var dateFromGeneric = function(year, month, day, time, offset){
	var d;
	if(isNaN(day)){
		if(day.indexOf('>=')==-1){
			// format = 'lastSun'
			day = daysPerWeek.indexOf(day.replace('last',''));
			d = new Date(year, month, 0);
			var s = (d.getDay() - day + (day > d.getDay() ? 7 : 0));
			d.setDate(d.getDate() - s);
		}
		else{
			// format = Sun>=1
			day = day.split('>=');
			var dpw = daysPerWeek.indexOf(day[0]);
			d = new Date(year+' '+month+' '+day[1]);
			if(dpw < d.getDay())
				d.setDate(d.getDate()+dpw+7-d.getDay());
		}
	}
	else
		d = new Date(year+' '+month+' '+day);

	d.setFullYear(year);

	time = timeComponents(time);

	d.setHours(time.hour);
	d.setMinutes(time.minute);
	d.setSeconds(time.second);

	/*
	// Convert universal time to std time
	if(time.type && time.type.toLowerCase() == 'u')
		d.setSeconds(d.getSeconds() + (offsetInSeconds(offset)));
	*/

	return d;
};

var selectDSTRule = function(rules, year, offset, prev){
	var ruleForYear = function(rules, year, offset){
		rules = rules.slice();

		rules = rules.map(function(r){
			r = extend({},r);

			r.from_year = isNaN(r.from_year) ? r.from_year : parseInt(r.from_year);
			r.to_year = isNaN(r.to_year) ? r.to_year : parseInt(r.to_year);
			r.month = typeof r.month == 'number' ? r.month : monthsPerYear(r.month);
			r.date = dateFromGeneric(year, r.month, r.day, r.time, offset);

			return r;
		}).filter(function(r){
			var rv = ((r.from_year <= year && ((!isNaN(r.to_year) && r.to_year >= year) || r.to_year == 'max')) || (r.from_year == year && (r.to_year == 'only')));
			return rv;
		});

		rules.sort(ruleSorter);
		return rules;
	};

	rules = ruleForYear(rules, year, offset);

	rules.forEach(function(r,i,rs){
		r.toDate = i > 0 ? rs[i-1].date : null;
		r.lastDiff = i < rs.length-1 ? rs[i+1].diff : null;

		if(!r.lastDiff){
			if(prev && prev.rule){
				if(typeof prev.rule == 'string')
					r.lastDiff = prev.rule;
				else{
					prev.rule = ruleForYear(prev.rule, year - 1, prev.offset);
					r.lastDiff = prev.rule[0] ? prev.rule[0].diff : '0';
				}
			}
			else
				r.lastDiff = '0';
		}

		// Convert rule std/dst time to utc
		var conversionOffset = 0;
		var type = (isNaN(r.time[r.time.length-1]) ? r.time[r.time.length-1] : 'w').toLowerCase();
		if(type == 's' || type == 'w')
			conversionOffset += offsetInSeconds(offset);
		if(type == 'w')
			conversionOffset += offsetInSeconds(r.lastDiff);
		r.date.setSeconds(r.date.getSeconds() - conversionOffset);
	});

	return rules.length > 0 ? rules : null;
};

var selectZoneRule = function(zones, rules, date, type){
	type = type || 'u';	
	zones = zones.slice();

	zones.sort(function(a,b){
		return a.dates.utc.from - b.dates.utc.from;
	});
	zones = zones.map(function(z,i,s){
		return {current: z, prev: i>0 ? s[i-1] : null};
	});
	zones = zones.filter(function(z){
		var zoneDate = type == 's' ? z.current.dates.std : type == 'w' ? z.current.dates.wc : z.current.dates.utc;
		return (date >= zoneDate.from && (!zoneDate.to || date < zoneDate.to));
	});

	var zone = zones[0];
	if(zone){
		var prev = zone.prev;
		zone = zone.current;

		if(prev){
			prev = {
				rule: prev.rule ? rules[prev.rule] ? rules[prev.rule].slice() : prev.rule : null,
				offset: prev.offset
			}
		}
		zone.rule = zone.rule ? rules[zone.rule] ? selectDSTRule(rules[zone.rule], date.getFullYear(), zone.offset, prev) : zone.rule : null;
	}

	return zone;
};

var stdToWallClockTime = function(time, date, lastDiff){
	var type = time[time.length-1].toLowerCase();
	if(type == 'u' || type == 's')
		date.setSeconds(date.getSeconds() + (offsetInSeconds(lastDiff)));
	return date;
};

// TODO: Remember to remove all date tostrings
var utcConvertor = function(date, z, diff){
	if(!date)
		return null;

	if(!diff){
		diff = '0';
		if(z.rule){
			if(typeof z.rule == 'string'){
				diff = z.rule;
			}
			else{
				var rules = z.rule.slice();

				diff = rules[rules.length-1].lastDiff;
				for(var i=rules.length-1;i>=0;--i){
					if(date >= rules[i].date)
						diff = rules[i].diff;
				}
			}
		}
	}
	var wcDate = new Date(date);
	wcDate.setSeconds(wcDate.getSeconds() + (offsetInSeconds(z.offset) + offsetInSeconds(diff)));

	return wcDate;
};

var UTCTimeToWC = function(date, z){
	return utcConvertor(date, z);
};

var UTCTimeToStd = function(date, z){
	return utcConvertor(date, z, '0');
};

var TimeToUTC = function(date, z, time){
	if(!date)
		return null;
	
	var utcDate = new Date(date);
	var type = time || 'w';
	type = (isNaN(type[type.length-1]) ? type[type.length-1] : 'w').toLowerCase();
	var conversionOffset = type == 'u' ? 0 : offsetInSeconds(z.offset);

	if(!z.rule || type == 's'){
		utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset));
	}
	else if(type == 'w'){
		if(typeof z.rule == 'string'){
			utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + offsetInSeconds(z.rule)));
		}
		else{
			for(var i=z.rule.length-1;i>=0;--i){
				utcDate = new Date(date);
				utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + offsetInSeconds(z.rule[i].lastDiff)));
				if(utcDate < z.rule[i].date)
					break;
				
				utcDate = new Date(date);
				utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + offsetInSeconds(z.rule[i].diff)));
				if(utcDate >= z.rule[i].date && (!z.rule[i].toDate || utcDate < z.rule[i].toDate))
					break;
			}

			z.rule.forEach(function(r){
				r.toDate = r.toDate ? r.toDate : null;
				r.date = r.date;
			});
		}
	}
	return utcDate;
};

var prepareZoneData = function(zones, rules){
	var zoneData = [];
	zones.forEach(function(z,idz,zoneList){
		z = extend({},z);

		z.of = z.from = z.from || minDate;
		z.ot = z.to;// = z.to || maxDate;

		var ruleName = z.rule;
		
		z.from = dateComponents(z.from);
		z.from = dateFromGeneric(z.from.year, z.from.month, z.from.day, z.from.hour+':'+z.from.minute+':'+z.from.second, z.offset);

		if(z.to){
			z.to = dateComponents(z.to);
			z.to = dateFromGeneric(z.to.year, z.to.month, z.to.day, z.to.hour+':'+z.to.minute+':'+z.to.second, z.offset);
		}

		var lastRule = idz > 0 ? zones[idz-1].rule : null;
		var lastOffset = idz > 0 ? zones[idz-1].offset : null;
		lastRule = lastRule ? rules[lastRule] || lastRule : null;
		z.rule = z.rule ? rules[z.rule] ? selectDSTRule(rules[z.rule], z.from.getFullYear(), z.offset, {rule: lastRule, offset: lastOffset}) : z.rule : null;

		// Convert zone std/dst to utc
		z.from = TimeToUTC(z.from, idz > 0 ? zoneList[idz-1] : z, z.of);
		z.to = TimeToUTC(z.to, z, z.ot);

		z.wc = {
			from: UTCTimeToWC(z.from, z),
			to: UTCTimeToWC(z.to, z)
		};

		z.std = {
			from: UTCTimeToStd(z.from, z),
			to: UTCTimeToStd(z.to, z)
		};

		z.from = z.from ? z.from : null;
		z.to = z.to ? z.to : null;

		z.dates = {
			utc: {
				from: z.from,
				to: z.to
			},
			std: {
				from: z.std.from,
				to: z.std.to
			},
			wc: {
				from: z.wc.from,
				to: z.wc.to
			}
		};
		
		z.rule = ruleName;

		delete z.from;
		delete z.to;
		delete z.wc;
		delete z.of;
		delete z.ot;
		delete z.std;

		zoneData.push(z);
	});

	return zoneData;
};

var convert = function(date, from, to, data){
	if(!date)
		return [new Error('Date required')];

	date = new Date(date);
	if(isNaN(date.getTime()))
		return [new Error('Invalid date')];

	var zoneData = {}, returnDates = [], zone;

	zoneData[from] = data.zones[from] ? prepareZoneData(data.zones[from], data.rules) : null;
	zoneData[to] = data.zones[to] ? prepareZoneData(data.zones[to], data.rules) : null;

	if(!zoneData[from])
		return [new Error(from + ' Timezone does not exist.')];
	if(!zoneData[to])
		return [new Error(to + ' Timezone does not exist.')];

	returnDates.push(from+': '+date.toString());

	zone = selectZoneRule(zoneData[from], data.rules, date, 'w');
	if(!zone)
		return [new Error('Invalid date in '+from+' timezone')];

	date = TimeToUTC(date, zone);
	returnDates.push('UTC: '+date.toString());

	zone = selectZoneRule(zoneData[to], data.rules, date);
	if(!zone)
		return [new Error('Invalid date in '+to+' timezone')];

	date = UTCTimeToWC(date, zone);
	returnDates.push(to+': '+date.toString());

	return returnDates;
};

var listen = function(data){
	(convert('2000 Jan 15 13:10:00', 'Africa/Juba', 'America/Chicago', data)).forEach(function(r){
		console.log(r);
	});

	/*
	var abc = {};

	abc = {
		'Africa/Juba': prepareZoneData(data.zones['Africa/Juba'], data.rules),
		'America/Chicago': prepareZoneData(data.zones['America/Chicago'], data.rules)
	};

	var date = new Date('2000 Jan 15 13:10:00');

	console.log('Asia/Kolkata WC: '+ date.toString());

	//Convert from Asia/Kolkata to utc
	var mzone = selectZoneRule(abc['Africa/Juba'], data.rules, date, 'w');
	if(!mzone)
		return console.log('No such time');

	date = TimeToUTC(date, mzone);
	console.log('UTC: '+ date.toString());

	mzone = selectZoneRule(abc['America/Chicago'], data.rules, date);
	date = UTCTimeToWC(date, mzone);
	console.log('America/Chicago WC: '+ date.toString());

	/*
	var tries = 100, now = Date.now();

	for(var i=0;i<tries;++i)
		abc = prepareZoneData(data.zones['Asia/Yerevan'], data.rules, 'Asia/Yerevan');

	console.log((Date.now()-now)/tries);
	console.log(process.memoryUsage());


	data.zones.forEach(function(zones,idx){
		abc[idx] = prepareZoneData(zones, data.rules);
	});	

	
	fs.writeFile(__dirname+'/sql/ruledzones.json', JSON.stringify(abc), function(err, data){
		console.log(err || 'OK');
	});
	*/
};

listen(data);