var pg = require('pg');
var connectionString = 'tcp://timeapi:qwaszxwesdxc@localhost/timedb';

var db = {
	_query: function(query, values, callback){
		if(!callback && values && typeof values == 'function')
			callback = values, values = null;
		pg.connect(connectionString, function(err, client, done){
			var callbackWrapper = function(err, result){
				done(); // Return client to pool
				callback(err, result);
			};
			if(err)
				callbackWrapper(err, null);
			else{
				if(values)
					client.query(query, values, callbackWrapper);
				else
					client.query(query, callbackWrapper);
			}
		});
	},
	closePool: function(){
		//Never call closePool in production
		//if(process.env.NODE_ENV == 'dev')
		pg.end();
	}
}
module.exports = db;