DROP FUNCTION insert_rule(name varchar, from_year varchar, to_year varchar, month varchar, day varchar, at_time varchar, diff varchar, letter varchar);
CREATE FUNCTION insert_rule(name varchar, from_year varchar, to_year varchar, month varchar, day varchar, at_time varchar, diff varchar, letter varchar) RETURNS json AS $$
	var result = {};
	if(!name || !from_year || !to_year || !month || !day || !at_time || !diff || !letter)
		return result;

	var monthIndex = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	month = (monthIndex.indexOf(month)) + 1;
	if(letter == '-')
		result.msg = plv8.execute('INSERT INTO rules (name, from_year, to_year, month, day, time, diff, letter) VALUES(\''+name+'\',\''+from_year+'\',\''+to_year+'\','+month+',\''+day+'\',\''+at_time+'\',\''+diff+'\',NULL);');
	else
		result.msg = plv8.execute('INSERT INTO rules (name, from_year, to_year, month, day, time, diff, letter) VALUES(\''+name+'\',\''+from_year+'\',\''+to_year+'\','+month+',\''+day+'\',\''+at_time+'\',\''+diff+'\',\''+letter+'\');');

	result.count = plv8.execute('SELECT COUNT(*) FROM rules;');	
	return result;
$$ LANGUAGE plv8;

DROP FUNCTION insert_zone(name varchar, def varchar[]);
CREATE FUNCTION insert_zone(name varchar, def varchar[]) RETURNS json AS $$
	var result = {};
	if(!name || !def)
		return result;

	var records = [], record = {}, last_date;
	def.forEach(function(e,i){
		var u = i%4;
		if(u==0){
			record = {};
			record.name = name;
			record.offset = e;
		}
		else if(u==1)
			record.rule = e == '-' ? null : e;
		else if(u==2)
			record.shortcode = e == '-' ? null : e;
		else if(u==3){
			record.to = e;
			record.from = last_date ? last_date : null;
			last_date = e;
			if(i!=0)
				records.push(record);
		}
	});
	record.from = last_date ? last_date : null;
	record.to = null;
	records.push(record);

	result.inserts = [];
	records.forEach(function(e){
		result.inserts.push(plv8.execute('INSERT INTO zones (name, utc_offset, rule, shortcode, from_date, to_date) VALUES(\''+e.name+'\',\''+e.offset+'\','+(e.rule?'\''+e.rule+'\'':'NULL')+','+(e.shortcode?'\''+e.shortcode+'\'':'NULL')+','+(e.from?'\''+e.from+'\'':'\'0\'')+','+(e.to?'\''+e.to+'\'':'NULL')+')'));
	});
	result.count = plv8.execute('SELECT count(*) FROM zones;');
	return result;
$$ LANGUAGE plv8;
