var fs = require('fs'),
	extend = require('./extend.js');

// Monkey patch Object
Object.prototype.forEach = function(fn){
	var obj = this;
	for(var i in obj){
		if(obj.hasOwnProperty(i))
			fn(obj[i],i,obj);
	}
};

//var daysPerMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
var daysPerWeek = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

var minDate = '0 Jan 1 0:00:00';
var maxDate = '9999 Dec 31 23:59:59';

var data = {
	rules: JSON.parse(fs.readFileSync(__dirname+'/sql/rules.json', {encoding: 'utf8'})),
	zones: JSON.parse(fs.readFileSync(__dirname+'/sql/zones.json', {encoding: 'utf8'}))
};

var names = [];
data.zones.forEach(function(e,i){
	names.push('<li class="tzname">'+i+'</li>');
});

fs.writeFileSync(__dirname+'/timezonenames', names.join('\n'), {encoding: 'utf8'});
