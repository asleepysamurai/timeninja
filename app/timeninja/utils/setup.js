;
var env = process.env.NODE_ENV || 'development';

var fs = require('fs'),
	exec = require('child_process').exec;

var config = require('../config/app.js'),
	logger = new (require('../utils/logger.js').Logger)();

var replaceString = function(source, stringStart, stringEnd, replaceWith){	
	var _index = source.indexOf(stringStart);
	var extract = source.substring(_index, source.indexOf(stringEnd, _index)) + ';';
	source = source.replace(extract, replaceWith);
	return {source: source, extract: extract};
};

var minifyStatics = function(){
	logger.info('Minifying static assets...');

	var js = require('uglify-js'),
		css = require('clean-css');

	var result = js.minify(config.minify.js.merge.map(function(f){
		return config.nginx.basedir+'/js/'+f+'.js';
	}));
	fs.writeFileSync(config.nginx.basedir+'/js/app.min.js', result.code, {encoding: 'utf8'});

	config.minify.js.split.forEach(function(f){
		result = js.minify(config.nginx.basedir+'/js/'+f+'.js');
		fs.writeFileSync(config.nginx.basedir+'/js/'+f+'.min.js', result.code, {encoding: 'utf8'});
	});

	var cssSource = [];
	config.minify.css.merge.forEach(function(f){
		cssSource.push(fs.readFileSync(config.nginx.basedir+'/css/'+f+'.css', {encoding: 'utf8'}));
	});
	cssSource = cssSource.join('\n');

	// Move @import directive to top of concated file
	var extract = replaceString(cssSource, '@import', ';', '');
	cssSource = extract.extract+extract.source;
	result = css.process(cssSource);

	fs.writeFileSync(config.nginx.basedir+'/css/app.min.css', result, {encoding: 'utf8'});
};

var configNginx = function(){
	// Configure nginx basedir
	logger.info('Configuring nginx...');
	var nginx = fs.readFileSync(config.nginx.configFile, {encoding: 'utf8'});
	var replaceWith = 'set $basedir "'+config.nginx.basedir+'";';
	nginx = (replaceString(nginx,'set $basedir',';',replaceWith));

	if(nginx.extract != replaceWith){
		logger.info('Rewriting nginx config...');
		fs.writeFileSync(config.nginx.configFile, nginx.source, {encoding: 'utf8'});
	}
	return nginx.extract != replaceWith;
};


module.exports = function(callback){
	// Common actions : perform in all environments
	var reload = configNginx();

	// Config for production only
	if('production' == env){
		// Minify CSS and JS
		minifyStatics();
	}

	// Reload nginx config
	if(reload){
		logger.info('Reloading nginx config...');
		exec('sudo /etc/init.d/nginx reload', function(err, stdout, stderr){
			if(err)
				logger.error('Failed to reload nginx config.');

			callback(err);
		});
	}
	else
		callback();
};