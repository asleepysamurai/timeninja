var fs = require('fs'),
	extend = require('./extend.js');

var daysPerWeek = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

var minDate = '0 Jan 1 0:00:00';
var maxDate = '9999 Dec 31 23:59:59';

var data = {
	rules: JSON.parse(fs.readFileSync(__dirname+'/../data/rules.json', {encoding: 'utf8'})),
	zones: JSON.parse(fs.readFileSync(__dirname+'/../data/zones.json', {encoding: 'utf8'}))
};

var monthsPerYear = function(idx){
	var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	if(typeof idx == 'number')
		return m[idx-1];
	else
		return m.indexOf(idx) + 1;
};

var ruleSorter = function(a,b){
	return b.date - a.date;
};

var timeComponents = function(time){
	var type, sign = 1;
	if(isNaN(time[0])){
		sign = -1;
		time = time.substr(1);
	}
	if(isNaN(time[time.length-1]))
		type = time[time.length-1];

	time = time.split(/:/g).map(function(t){ return parseInt(t); });
	for(var i=time.length;i<3;++i)
		time[i] = 0;

	return {
		sign: sign,
		hour: time[0] ? time[0] : 0,
		minute: time[1] ? time[1] : 0,
		second: time[2] ? time[2] : 0,
		type: type
	};
};

var dateComponents = function(date){
	date = date.split(/\s/g);
	
	var time = date[3] ? date[3] : '00:00:00', type;	

	date = {
		year: date[0] ? parseInt(date[0]) : null,
		month: date[1] ? monthsPerYear(date[1]) : 1,
		day: date[2] ? date[2] : 1,
	};

	time = timeComponents(time);
	date.hour = time.hour;
	date.minute = time.minute;
	date.second = time.second;
	date.type = time.type;

	return date;
};

var timeInSeconds = function(offset){
	offset = timeComponents(offset);
	return offset.sign * ((offset.hour * 60 * 60) + (offset.minute * 60) + offset.second);
};

var dateFromGeneric = function(year, month, day, time, offset){
	var d;
	if(isNaN(day)){
		if(day.indexOf('>=')==-1){
			// format = 'lastSun'
			day = daysPerWeek.indexOf(day.replace('last',''));
			d = new Date(year, month, 0);
			var s = (d.getDay() - day + (day > d.getDay() ? 7 : 0));
			d.setDate(d.getDate() - s);
		}
		else{
			// format = Sun>=1
			day = day.split('>=');
			var dpw = daysPerWeek.indexOf(day[0]);
			d = new Date(year+' '+month+' '+day[1]);
			if(dpw < d.getDay())
				d.setDate(d.getDate()+dpw+7-d.getDay());
		}
	}
	else
		d = new Date(year+' '+month+' '+day);

	d.setFullYear(year);

	time = timeComponents(time);

	d.setHours(time.hour);
	d.setMinutes(time.minute);
	d.setSeconds(time.second);

	/*
	// Convert universal time to std time
	if(time.type && time.type.toLowerCase() == 'u')
		d.setSeconds(d.getSeconds() + (timeInSeconds(offset)));
	*/

	return d;
};

var selectDSTRule = function(rules, year, offset, prev){
	var ruleForYear = function(rules, year, offset){
		rules = rules.slice();

		rules = rules.map(function(r){
			r = extend({},r);

			r.from_year = isNaN(r.from_year) ? r.from_year : parseInt(r.from_year);
			r.to_year = isNaN(r.to_year) ? r.to_year : parseInt(r.to_year);
			r.month = typeof r.month == 'number' ? r.month : monthsPerYear(r.month);
			r.date = dateFromGeneric(year, r.month, r.day, r.time, offset);

			return r;
		}).filter(function(r){
			var rv = ((r.from_year <= year && ((!isNaN(r.to_year) && r.to_year >= year) || r.to_year == 'max')) || (r.from_year == year && (r.to_year == 'only')));
			return rv;
		});

		rules.sort(ruleSorter);
		return rules;
	};

	rules = ruleForYear(rules, year, offset);

	rules.forEach(function(r,i,rs){
		r.toDate = i > 0 ? rs[i-1].date : null;
		r.lastDiff = i < rs.length-1 ? rs[i+1].diff : null;

		if(!r.lastDiff){
			if(prev && prev.rule){
				if(typeof prev.rule == 'string')
					r.lastDiff = prev.rule;
				else{
					prev.rule = ruleForYear(prev.rule, year - 1, prev.offset);
					r.lastDiff = prev.rule[0] ? prev.rule[0].diff : '0';
				}
			}
			else
				r.lastDiff = '0';
		}

		// Convert rule std/dst time to utc
		var conversionOffset = 0;
		var type = (isNaN(r.time[r.time.length-1]) ? r.time[r.time.length-1] : 'w').toLowerCase();
		if(type == 's' || type == 'w')
			conversionOffset += timeInSeconds(offset);
		if(type == 'w')
			conversionOffset += timeInSeconds(r.lastDiff);
		r.date.setSeconds(r.date.getSeconds() - conversionOffset);
	});

	return rules.length > 0 ? rules : null;
};

var selectZoneRule = function(zones, rules, date, type){
	type = type || 'u';	
	zones = zones.slice();

	zones.sort(function(a,b){
		return a.dates.utc.from - b.dates.utc.from;
	});
	zones = zones.map(function(z,i,s){
		return {current: z, prev: i>0 ? s[i-1] : null};
	});
	zones = zones.filter(function(z){
		var zoneDate = type == 's' ? z.current.dates.std : type == 'w' ? z.current.dates.wc : z.current.dates.utc;
		return (date >= zoneDate.from && (!zoneDate.to || date < zoneDate.to));
	});

	var zone = zones[0];
	if(zone){
		var prev = zone.prev;
		zone = zone.current;

		if(prev){
			prev = {
				rule: prev.rule ? rules[prev.rule] ? rules[prev.rule].slice() : prev.rule : null,
				offset: prev.offset
			}
		}
		zone.rule = zone.rule ? rules[zone.rule] ? selectDSTRule(rules[zone.rule], date.getFullYear(), zone.offset, prev) : zone.rule : null;
	}

	return zone;
};

var stdToWallClockTime = function(time, date, lastDiff){
	var type = time[time.length-1].toLowerCase();
	if(type == 'u' || type == 's')
		date.setSeconds(date.getSeconds() + (timeInSeconds(lastDiff)));
	return date;
};

var padString = function(string, length, padder){
	string = string.toString();
	var padding = '', padder = padder || '0';
	for(var i=string.length;i<length;++i)
		padding += padder;
	return (padding+string).substr(-1*length);
};

var secondsToTime = function(seconds){
	var d = {
		h: parseInt(seconds/(60*60)),
		m: parseInt((seconds%(60*60))/60),
		s: parseInt((seconds%(60*60))%60)
	}

	return padString(d.h,2)+':'+padString(d.m,2)+(d.s > 0 ? ':'+padString(d.s,2) : '');
};

var utcConvertor = function(date, z, diff){
	if(!date)
		return {date: null, offset: null};

	if(!diff){
		diff = '0';
		if(z.rule){
			if(typeof z.rule == 'string'){
				diff = z.rule;
			}
			else{
				var rules = z.rule.slice();

				diff = rules[rules.length-1].lastDiff;
				for(var i=rules.length-1;i>=0;--i){
					if(date >= rules[i].date)
						diff = rules[i].diff;
				}
			}
		}
	}
	var wcDate = new Date(date), offset = timeInSeconds(z.offset) + timeInSeconds(diff);
	wcDate.setSeconds(wcDate.getSeconds() + offset);

	return {date: wcDate, offset: secondsToTime(offset)};
};

var UTCTimeToWC = function(date, z){
	return utcConvertor(date, z);
};

var UTCTimeToStd = function(date, z){
	return utcConvertor(date, z, '0');
};

var TimeToUTC = function(date, z, time){
	if(!date)
		return null;
	
	var utcDate = new Date(date);
	var type = time || 'w';
	type = (isNaN(type[type.length-1]) ? type[type.length-1] : 'w').toLowerCase();
	var conversionOffset = type == 'u' ? 0 : timeInSeconds(z.offset);

	if(!z.rule || type == 's'){
		utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset));
	}
	else if(type == 'w'){
		if(typeof z.rule == 'string'){
			utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + timeInSeconds(z.rule)));
		}
		else{
			for(var i=z.rule.length-1;i>=0;--i){
				utcDate = new Date(date);
				utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + timeInSeconds(z.rule[i].lastDiff)));
				if(utcDate < z.rule[i].date)
					break;
				
				utcDate = new Date(date);
				utcDate.setSeconds(utcDate.getSeconds() - (conversionOffset + timeInSeconds(z.rule[i].diff)));
				if(utcDate >= z.rule[i].date && (!z.rule[i].toDate || utcDate < z.rule[i].toDate))
					break;
			}

			z.rule.forEach(function(r){
				r.toDate = r.toDate ? r.toDate : null;
				r.date = r.date;
			});
		}
	}
	return utcDate;
};

var prepareZoneData = function(zones, rules){
	var zoneData = [];
	zones.forEach(function(z,idz,zoneList){
		z = extend({},z);

		z.of = z.from = z.from || minDate;
		z.ot = z.to;// = z.to || maxDate;

		var ruleName = z.rule;
		
		z.from = dateComponents(z.from);
		z.from = dateFromGeneric(z.from.year, z.from.month, z.from.day, z.from.hour+':'+z.from.minute+':'+z.from.second, z.offset);

		if(z.to){
			z.to = dateComponents(z.to);
			z.to = dateFromGeneric(z.to.year, z.to.month, z.to.day, z.to.hour+':'+z.to.minute+':'+z.to.second, z.offset);
		}

		var lastRule = idz > 0 ? zones[idz-1].rule : null;
		var lastOffset = idz > 0 ? zones[idz-1].offset : null;
		lastRule = lastRule ? rules[lastRule] || lastRule : null;
		z.rule = z.rule ? rules[z.rule] ? selectDSTRule(rules[z.rule], z.from.getFullYear(), z.offset, {rule: lastRule, offset: lastOffset}) : z.rule : null;

		// Convert zone std/dst to utc
		z.from = TimeToUTC(z.from, idz > 0 ? zoneList[idz-1] : z, z.of);
		z.to = TimeToUTC(z.to, z, z.ot);

		z.wc = {
			from: UTCTimeToWC(z.from, z).date,
			to: UTCTimeToWC(z.to, z).date
		};

		z.std = {
			from: UTCTimeToStd(z.from, z).date,
			to: UTCTimeToStd(z.to, z).date
		};

		z.from = z.from ? z.from : null;
		z.to = z.to ? z.to : null;

		z.dates = {
			utc: {
				from: z.from,
				to: z.to
			},
			std: {
				from: z.std.from,
				to: z.std.to
			},
			wc: {
				from: z.wc.from,
				to: z.wc.to
			}
		};
		
		z.rule = ruleName;

		delete z.from;
		delete z.to;
		delete z.wc;
		delete z.of;
		delete z.ot;
		delete z.std;

		zoneData.push(z);
	});

	return zoneData;
};

var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

var isLeapYear = function(year){
	return year%400 == 0 || (year%4 == 0 && year%100 != 0);
};

var isValidDate = function(date){
	return dateFromObject(date) instanceof Date;
};

var dateFromObject = function(date){
	var jsDate = new Date(date.Y+'-'+date.M+'-'+date.D+' '+date.h+':'+date.m+':'+date.s);
	
	if(isNaN(jsDate.getTime()))
		return null;

	var maxDays = [31,isLeapYear(date.Y) ? 29 : 28,31,30,31,30,31,31,30,31,30,31][date.M-1];
	if(!maxDays || maxDays < date.D)
		return null;

	return jsDate;
};

var parseDateString = function(date){
	if(!date)
		return false;

	var dateObj = {};

	date = date.split(/\s/g).filter(function(d){ return d.length != 0; });

	if(date.length < 2 || date.length > 3)
		return false;

	if(isNaN((new Date(date[0]+' '+date[1])).getTime()))
		return false;

	if(date[0].indexOf('-') == -1 || date[1].indexOf(':') == -1)
		return false;

	date[0] = date[0].split('-').filter(function(d){ return !isNaN(d); }).map(function(d){ return parseInt(d); });
	date[1] = date[1].split(':').filter(function(d){ return !isNaN(d); }).map(function(d){ return parseInt(d); });

	if(date[0].length != 3 || date[1].length != 3)
		return false;

	if(date[0].length == 3)
		dateObj = { D: date[0][2], M: date[0][1], Y: date[0][0] };

	if(date[1].length == 3){
		dateObj.h = date[1][0];
		dateObj.m = date[1][1];
		dateObj.s = date[1][2];
	}
	
	if(date[2])
		dateObj.z = date[2];

	return dateObj;
};

var isValidDateObject = function(date, timeOnly){
	if(!date)
		return false;

	if((timeOnly ? ['h','m','s'] : ['M','D','Y','h','m','s']).filter(function(p){ return date[p] === undefined; }).length > 0)
		return false;

	if(!date.z)
		date.z == 'UTC';

	return date;
};

var dayFromIndex = function(index){
	return daysOfWeek[index];
};

var isoIndexFromDay = function(day){
	day = daysOfWeek.indexOf(day);
	return day == 0 ? 7 : day;
};

var shortDayFromDay = function(day){
	return day.substr(0,3);
};

var dayOfDate = function(date){
	var result = {};
	result.day_index = date.getDay();
	result.day = dayFromIndex(result.day_index);
	result.iso_day_index = isoIndexFromDay(result.day);
	result.short_day = shortDayFromDay(result.day);
	return result;
};

var dateObjectFromDate = function(date, tz, offset){
	return {
		d: date.getDay(),
		Y: date.getFullYear(),
		M: 1+date.getMonth(),
		D: date.getDate(),
		h: date.getHours(),
		m: date.getMinutes(),
		s: date.getSeconds(),
		z: tz || 'UTC',
		o: offset
	};
};

var convert = function(date, from, to, jsDate){
	if(!date)
		return [new Error('Date required')];

	date = date instanceof Date ? date : dateFromObject(date);
	if(!date)
		return new Error('Invalid date');

	from = from == 'UTC' ? null : from;
	to = to == 'UTC' ? null : to;

	var zoneData = {}, returnDates = {}, zone;
	var utc, wc, std;	

	zoneData[from] = from && data.zones[from] ? prepareZoneData(data.zones[from], data.rules) : null;
	zoneData[to] = to && data.zones[to] ? prepareZoneData(data.zones[to], data.rules) : null;

	if(from && !zoneData[from])
		return new Error(from + ' Timezone does not exist.');
	if(to && !zoneData[to])
		return new Error(to + ' Timezone does not exist.');

	if(from){
		zone = selectZoneRule(zoneData[from], data.rules, date, 'w');
		if(!zone)
			return new Error('Invalid date in '+from+' timezone.');

		utc = TimeToUTC(date, zone);
	}
	else
		utc = date;
	returnDates.utc = jsDate ? utc : dateObjectFromDate(utc, 'UTC', 'Z');

	if(to){
		zone = selectZoneRule(zoneData[to], data.rules, utc);
		if(!zone)
			return new Error('Invalid date in '+to+' timezone.	');

		wc = UTCTimeToWC(utc, zone);
		std = UTCTimeToStd(utc, zone);
	}
	else{
		wc = {date: utc, offset: 'Z'};
		std = {date: utc, offset: 'Z'};
	}

	returnDates.wallclock = jsDate ? wc.date : dateObjectFromDate(wc.date, to, wc.offset);
	returnDates.standard = jsDate ? std.date : dateObjectFromDate(std.date, to, std.offset);

	return returnDates;
};

var formatDateObject = function(date, formatString){

	formatString = formatString.replace('YYYY', padString(date.Y, 4));
	formatString = formatString.replace(/YY?/, padString(date.Y, 2));

	formatString = formatString.replace('MM', padString(date.M, 2));
	formatString = formatString.replace('M', date.M.toString());

	formatString = formatString.replace('DD', padString(date.D, 2));
	formatString = formatString.replace('D', date.D.toString());

	formatString = formatString.replace('hh', padString(date.h, 2));
	formatString = formatString.replace('h', date.h.toString());

	formatString = formatString.replace('mm', padString(date.m, 2));
	formatString = formatString.replace('m', date.m.toString());

	formatString = formatString.replace('ss', padString(date.s, 2));
	formatString = formatString.replace('s', date.s.toString());

	formatString = formatString.replace('ddd', daysOfWeek[date.d] ? daysOfWeek[date.d].substr(0,3) : null);
	formatString = formatString.replace('d', daysOfWeek[date.d] || null);

	// Edge case ddd and wed becomes wewednesday
	formatString = formatString.replace('WeWednesday', 'Wed');

	formatString = formatString.replace('o', isNaN(date.o[0]) ? date.o : '+'+date.o);
	formatString = formatString.replace('z', date.z);

	return formatString;
};

var convertAndFormat = function(date, from, to, formatString){
	formatString = formatString || 'YYYY-MM-DDThh:mm:ss.00o';
	date = convert(date, from, to);

	return date instanceof Error ? date : {
		utc: formatDateObject(date.utc, formatString),
		standard: formatDateObject(date.standard, formatString),
		wallclock: formatDateObject(date.wallclock, formatString)
	};
};

var add = function(date, time){
	date = dateFromObject(date);
	if(!date)
		return new Error('Invalid date');

	date.setSeconds(date.getSeconds()+(time.sign*timeInSeconds((time.h||'0')+':'+(time.m||'0')+':'+time.s+('0'))));
	return date;
};

var addAndFormat = function(date, time, formatString){
	var dz = date.z, tz = time.z || dz;
	formatString = formatString || 'YYYY-MM-DDThh:mm:ss.00o';

	date = convert(date, dz, 'UTC').utc;
	date = add(date, time);
	return convertAndFormat(date, 'UTC', tz, formatString);
};

var parseTimeString = function(time){
	if(!time)
		return false;

	time = time.split(/\s/).filter(function(t){ return t.length != 0; });
	if(time.length < 1 || time.length > 2)
		return false;

	if(time[0].indexOf(':') == -1)
		return false;

	var sign = time[0][0] == '-' ? -1 : 1;
	
	var timeObj = {};
	time[0] = time[0].split(':').filter(function(t){ return t.length != 0; }).map(function(t){ t = parseInt(t); return t < 0 ? -1*t : t;});

	
	if(time[0].length == 1)
		timeObj.s = {s: time[0][0], h: 0, m: 0};
	else if(time[0].length == 2)
		timeObj = {s: time[0][1], m: time[0][0], h: 0};
	else if(time[0].length == 3)
		timeObj = {s: time[0][2], m: time[0][1], h: time[0][0]};

	if(time[1])
		timeObj.z = time[1];

	timeObj.sign = sign;

	return timeObj;
};

var diffAndFormat = function(date1, date2){
	var d1z = date1.z, d2z = date2.z;
	date1 = convert(date1, d1z, 'UTC', true);
	date2 = convert(date2, d2z, 'UTC', true);
	
	if(date1 instanceof Error)
		return date1;
	if(date2 instanceof Error)
		return date2;

	return {diff: parseInt((date1.utc - date2.utc)/1000)};
};

module.exports = {
	convert: convertAndFormat,
	add: addAndFormat,
	diff: diffAndFormat,
	parseDateString: parseDateString,
	parseTimeString: parseTimeString,
	isValidDate: isValidDate,
	isValidDateObject: isValidDateObject,
	dateFromObject: dateFromObject,
	dayOfDate: dayOfDate
};