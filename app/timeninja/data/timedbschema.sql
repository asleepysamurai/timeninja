--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: email_change; Type: TABLE; Schema: public; Owner: timeapi_prod; Tablespace: 
--

CREATE TABLE email_change (
    code character varying,
    expires bigint,
    email character varying,
    new_email character varying NOT NULL
);


ALTER TABLE public.email_change OWNER TO timeapi_prod;

--
-- Name: reset; Type: TABLE; Schema: public; Owner: timeapi_prod; Tablespace: 
--

CREATE TABLE reset (
    code character varying,
    expires bigint,
    email character varying
);


ALTER TABLE public.reset OWNER TO timeapi_prod;

--
-- Name: users; Type: TABLE; Schema: public; Owner: timeapi_prod; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying,
    pass_hash character varying,
    api_key character varying,
    api_secret character varying,
    plan integer,
    verified boolean DEFAULT false,
    joined_at bigint,
    updated_at bigint,
    usage integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.users OWNER TO timeapi_prod;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: timeapi_prod
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO timeapi_prod;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: timeapi_prod
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: verification; Type: TABLE; Schema: public; Owner: timeapi_prod; Tablespace: 
--

CREATE TABLE verification (
    code character varying,
    expires bigint,
    email character varying
);


ALTER TABLE public.verification OWNER TO timeapi_prod;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: timeapi_prod
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: apikey_users_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT apikey_users_ukey UNIQUE (api_key);


--
-- Name: code_email_change_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY email_change
    ADD CONSTRAINT code_email_change_ukey UNIQUE (code);


--
-- Name: code_reset_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY reset
    ADD CONSTRAINT code_reset_ukey UNIQUE (code);


--
-- Name: code_verification_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY verification
    ADD CONSTRAINT code_verification_ukey UNIQUE (code);


--
-- Name: ekey_users_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT ekey_users_ukey UNIQUE (api_secret);


--
-- Name: email_email_change_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY email_change
    ADD CONSTRAINT email_email_change_ukey UNIQUE (email);


--
-- Name: email_reset_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY reset
    ADD CONSTRAINT email_reset_ukey UNIQUE (email);


--
-- Name: email_users_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT email_users_ukey UNIQUE (email);


--
-- Name: email_verification_ukey; Type: CONSTRAINT; Schema: public; Owner: timeapi_prod; Tablespace: 
--

ALTER TABLE ONLY verification
    ADD CONSTRAINT email_verification_ukey UNIQUE (email);


--
-- Name: email_reset_fkey; Type: FK CONSTRAINT; Schema: public; Owner: timeapi_prod
--

ALTER TABLE ONLY reset
    ADD CONSTRAINT email_reset_fkey FOREIGN KEY (email) REFERENCES users(email) ON UPDATE CASCADE;


--
-- Name: email_reset_fkey; Type: FK CONSTRAINT; Schema: public; Owner: timeapi_prod
--

ALTER TABLE ONLY email_change
    ADD CONSTRAINT email_reset_fkey FOREIGN KEY (email) REFERENCES users(email) ON UPDATE CASCADE;


--
-- Name: email_verification_fkey; Type: FK CONSTRAINT; Schema: public; Owner: timeapi_prod
--

ALTER TABLE ONLY verification
    ADD CONSTRAINT email_verification_fkey FOREIGN KEY (email) REFERENCES users(email) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

