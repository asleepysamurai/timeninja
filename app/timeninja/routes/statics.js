
/*
 * All static pages
 */

var randomInt = function(max, min){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

var landingPage = function(req, res){
	var fact = ['Did you know that 15th January 2000, 12:30PM did not exist in South Sudan?',
				'Did you know that the DST offset in Lord Howe Island is only half an hour?',
				'Did you know that the USA did not observe standard time between February 1942 and September 1945?'][(parseInt((Math.random())*100))%3];

	res.render('index', {title: 'Super simple time API', fact: fact});
};

exports.route = function(app){
	// Home page
	app.get('/', landingPage);
	app.get('/index', landingPage);

	// Pricing page
	app.get('/pricing', function(req, res){
		res.render('pricing', {title: 'Pricing', nocharge: true});
	});

	// TOS page
	app.get('/terms', function(req, res){
		res.render('terms', {title: 'Terms of Service'});
	});

	// Privacy page
	app.get('/privacy', function(req, res){
		res.render('privacy', {title: 'Privacy Policy'});
	});

	// Docs
	app.get('/docs', function(req, res){
		res.render('docs', {title: 'Documentation'});
	});

	// Docs
	app.get('/docs/timezones', function(req, res){
		res.render('timezones', {title: 'Documentation - Supported Timezones'});
	});
};