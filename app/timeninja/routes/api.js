
/*
 * API routes
 */

var crypto = require('crypto');

var time = require('../utils/time.js'),
	db = require('../utils/db.js'),
	extend = require('../utils/extend.js'),
	plans = require('../config/app.js').plans;

var logger = new (require('../utils/logger.js').Logger)();

var dateObjectFromQuery = function(date, req, res, timeOnly){
	var error, td = (timeOnly?'time':'date');
	var getObject = function(date){
		if(typeof date == 'string')
			date = timeOnly ? time.parseTimeString(date) : time.parseDateString(date);

		if(!time.isValidDateObject(date, timeOnly)){
			error = {error: 'Invalid '+td+' format', message: 'Invalid '+td+' format. The '+td+' you provided did not confirm to the '+td+' format expected. Please review the documentation at http://www.timeninja.net/docs#ab-400 for valid '+td+' formats.'};
			return null;
		}

		if(!timeOnly && !time.isValidDate(date)){
			error = {error: 'Invalid '+td, message: 'Invalid '+td+'. The '+td+' you provided does not exist. Please review the documentation at http://www.timeninja.net/docs#ab-400 for valid '+td+'s.'};
			return null;
		}

		return date;
	};

	var validDates = getObject(date);
	if(validDates)
		return validDates;

	error.code = 400;
	res.status(400).json(extend(res.locals.data, error));
	return null;
};

var updateUsage = function(usage, id){
	usage.available = usage.max - (++usage.used);
	db._query('UPDATE users SET usage = usage + 1 WHERE id = $1', [id], function(err, results){
		if(err)
			logger.error('Error while trying to increment request count. '+err+'. '+id);
	});
};

var arithmetic = function(req, res, next, neg){
	if(!req.query.date || !req.query.time)
		return res.status(400).json(extend(res.locals.data, {code: 400, error: 'Required parameter missing', message: 'Required parameter missing. Check the documentation at http://www.timeninja.net/docs#ab-400 for mandatory api inputs.'}));

	var date = dateObjectFromQuery(req.query.date, req, res);
	if(!date)
		return null;

	var _time = dateObjectFromQuery(req.query.time, req, res, true);
	if(!_time)
		return null;

	if(neg)
		_time.sign *= -1;

	var result = time.add(date, _time, req.query.format);

	if(result instanceof Error){
		var msg = result.message.indexOf('Invalid') != -1 
			? ' This may be due to a change due to daylight savings time or due to a change of the standard time zone offset.'
			: ' Check the documentation at http://www.timeninja.net/docs/timezones for a list of valid timezones.';

		return res.status(400).json(extend(res.locals.data, {code: 400, error: result.message, message: result.message+msg}));
	}

	updateUsage(res.locals.data.usage, res.locals.api_user);
		
	return res.status(200).json(extend(res.locals.data, {code: 200, result: result}));
};

// Middleware for authorizing all api requests
exports.auth = function(req, res, next){
	if(req.path.indexOf('/api') != 0)
		return next();

	if(!req.param('api_key'))
		return res.status(400).json({code: 400, error: 'API key required', message: 'API key required. You must send an api_key parameter with any api request. Login to your dashboard to find out your API key.'});
	/*
	if(!req.param('secret_data'))
		return res.status(400).json({code: 400, error: 'Secret data required', message: 'Secret data required. You must send a secret_data parameter with any api request. This can be any string of any length. It is recommended to generate a new random string for each API request.'});
	if(!req.param('secret_hash'))
		return res.status(400).json({code: 400, error: 'Secret hash required', message: 'Secret hash required. You must send an secret_hash parameter with any api request. To generate secret_hash concatenate your api_secret and the secret_data and md5 hash the resulting string. Note: never send api_secret with any API request. API secret is to be kept secret.'});
	*/
	db.select('users', ['*'], ['api_key'], req.param('api_key'), null, function(err, results){
		if(err || !results.rows || !results.rows[0])
			return res.status(401).json({code: 401, error: 'Invalid API key', message: 'Invalid API key. You must send a valid api_key parameter with any api request. Login to your dashboard to find out your API key.'});
		if(!results.rows[0].verified)
			return res.status(401).json({code: 401, error: 'Account not verified', message: 'Account not verified. The owner of this api_key has not verified their email address yet. Login to your email and verify your account.'});
		/*
		if(crypto.createHash('md5').update(results.rows[0].api_secret+req.param('secret_data')).digest('hex') != req.param('secret_hash'))
			return res.status(401).json({code: 401, error: 'Secret data/hash mismatch', message: 'Secret data/hash mismatch. secret_data can be any string of any length. It is recommended to generate a new random string for each API request. To generate secret_hash concatenate your api_secret and the secret_data and md5 hash the resulting string. Note: never send api_secret with any API request. API secret is to be kept secret.'});
		*/
		res.locals.data = {usage: {max: plans[results.rows[0].plan].requests.max, used: results.rows[0].usage}};

		res.locals.api_user = results.rows[0].id;

		next();
	});
};

exports.route = function(app){	
	// Get day of particular date
	app.get('/api/day', function(req, res, next){
		if(!req.query.date)
			return res.status(400).json(extend(res.locals.data, {code: 400, error: 'Required parameter missing', message: 'Required parameter missing. Check the documentation at http://www.timeninja.net/docs#ab-400 for mandatory api inputs.'}));

		req.query.date = dateObjectFromQuery(req.query.date, req, res);
		if(!req.query.date)
			return null;

		req.query.date = time.dateFromObject(req.query.date);

		updateUsage(res.locals.data.usage, res.locals.api_user);

		return res.status(200).json(extend(res.locals.data, {code: 200, result: time.dayOfDate(req.query.date)}));
	});

	// Convert between two timezones
	app.get('/api/convert', function(req, res, next){
		if(!req.query.from)
			return res.status(400).json(extend(res.locals.data, {code: 400, error: 'Required parameter missing', message: 'Required parameter missing. Check the documentation at http://www.timeninja.net/docs#ab-400 for mandatory api inputs.'}));

		var date = dateObjectFromQuery(req.query.from, req, res);
		if(!date)
			return null;

		var result = time.convert(date, date.z, req.query.to, req.query.format);

		if(result instanceof Error){
			var msg = result.message.indexOf('Invalid') != -1 
				? ' This may be due to a change due to daylight savings time or due to a change of the standard time zone offset.'
				: ' Check the documentation at http://www.timeninja.net/docs/timezones for a list of valid timezones.';

			return res.status(400).json(extend(res.locals.data, {code: 400, error: result.message, message: result.message+msg}));
		}

		updateUsage(res.locals.data.usage, res.locals.api_user);
		
		return res.status(200).json(extend(res.locals.data, {code: 200, result: result}));
	});

	// Add/subtract time to date
	app.get('/api/add', function(req, res, next){
		arithmetic(req, res, next);
	});
	app.get('/api/subtract', function(req, res, next){
		arithmetic(req, res, next, true);
	});

	// date diff
	app.get('/api/diff', function(req, res, next){
		if(!req.query.date1 || !req.query.date2)
			return res.status(400).json(extend(res.locals.data, {code: 400, error: 'Required parameter missing', message: 'Required parameter missing. Check the documentation at http://www.timeninja.net/docs#ab-400 for mandatory api inputs.'}));

		var date1 = dateObjectFromQuery(req.query.date1, req, res);
		if(!date1)
			return null;
		var date2 = dateObjectFromQuery(req.query.date2, req, res);
		if(!date2)
			return null;

		var result = time.diff(date1, date2);
		if(result instanceof Error){
			var msg = result.message.indexOf('Invalid') != -1 
				? ' This may be due to a change due to daylight savings time or due to a change of the standard time zone offset.'
				: ' Check the documentation at http://www.timeninja.net/docs/timezones for a list of valid timezones.';

			return res.status(400).json(extend(res.locals.data, {code: 400, error: result.message, message: result.message+msg}));
		}

		updateUsage(res.locals.data.usage, res.locals.api_user);
		
		return res.status(200).json(extend(res.locals.data, {code: 200, result: result}));
	});

	// 404 unknown endpoints for any method
	app.all('/api/*', function(req, res){
		return res.status(404).json(extend(res.locals.data, {code: 404, error: 'Invalid API endpoint', message: 'Invalid API endpoint. The endpoint you requested does not exist. Please review documentation for valid endpoints.'}));
	});
};