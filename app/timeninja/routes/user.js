
/*
 * All account related pages
 */

var crypto = require('crypto'),
	bcrypt = require('bcrypt');

var db = require('../utils/db'),
	mail = require('../utils/mail');

var plans = require('../config/app.js').plans;

var logger = new (require('../utils/logger.js').Logger)();

var createKey = function(){
	return crypto.randomBytes(16).toString('hex');
};

var checkAccess = function(req, res, next){
	if(!req.session || !req.session.uid)
		return res.redirect(302, '/login');
	next();
};

var destroyKey = function(type, email){
	db.remove(type, ['email'], [email], function(err, results){
		if(err)
			logger.error('Error occurred while destroying used reset code for email '+email+'.');
	});
};

var isEmailValid = function(email){
	var matches = email.match(/[\w\.+]+@\w+\.[\w\.]+/g);
	return (matches && matches[0] && matches[0] == email);
};

exports.route = function(app){
	app.get('/user/change-password', function(req, res){
		var locals = {title: 'Change account password', new1: null, new2: null};
		if(!req.query.code)
			return res.render('change_password', res.locals.put(locals,'error','reset_code_required'));

		return res.render('change_password', res.locals.put(locals,'code',req.query.code));
	});

	app.post('/user/change-password', function(req, res){
		var locals = {title: 'Change account password', code: req.body.code, new1: req.body.new1, new2: req.body.new2};
		if(!req.body.code)
			return res.render('change_password', res.locals.put(locals,'error','reset_code_required'));
		if(!req.body.new1)
			return res.render('change_password', res.locals.put(locals,'error','pass_required','error_type','pass1'));
		if(!req.body.new2)
			return res.render('change_password', res.locals.put(locals,'error','pass_required','error_type','pass2'));
		if(req.body.new1 != req.body.new2)
			return res.render('change_password', res.locals.put(locals,'error','password_mismatch','error_type','pass1'));

		db.select('reset', ['*'], ['code'], [req.body.code], null, function(err, results){
			if(err || !results.rows || !results.rows[0])
				return res.render('change_password', res.locals.put(locals,'error','reset_code_invalid'));

			if(Date.now() > results.rows[0].expires)
				return res.render('change_password', res.locals.put(locals,'error','reset_code_expired','email',results.rows[0].email));

			bcrypt.hash(req.body.new1, 16, function(err, hash){
				db.update('users', ['pass_hash', 'updated_at'], [hash, Date.now()], ['email'], [results.rows[0].email], function(err, results){
				if(err || !results.rows || !results.rows[0])
						return res.render('change_password', res.locals.put(locals,'error','server'));

					destroyKey('reset', results.rows[0].email);

					return res.render('change_password', res.locals.put(locals,'email',results.rows[0].email,'success',true));
				});
			});
		});
	});

	// User account actions
	app.get('/user/dashboard', checkAccess, function(req, res){
		db.select('users', ['*'], ['id'], [req.session.uid], null, function(err, results){
			if(err || !results.rows || !results.rows[0])
				return res.render('dashboard', {title: 'Dashboard', error: 'server'});

			var plan = plans[results.rows[0].plan];
			plan = { name: plan.name, requests: {max: plan.requests.max, current: results.rows[0].usage}, trial: (Date.now() - results.rows[0].joined_at) <= 2592000000}; // 2592000000 == 30 days
			res.render('dashboard', {title: 'Dashboard', api: { key: results.rows[0].api_key, secret: results.rows[0].api_secret }, plan: plan, footer: 'docs'});
		});
	});

	// Change user email
	app.get('/user/change-email', checkAccess, function(req, res){
		res.render('change_email', {title: 'Change Account Email', email: req.session.email});
	});

	app.post('/user/change-email', checkAccess, function(req, res){
		var locals = {title: 'Change Account Email', email: req.body.email || req.session.email };
		if(!req.body.email)
			return res.render('change_email', res.locals.put(locals,'error','email_required'));
		if(!isEmailValid(req.body.email))
			return res.render('change_email', res.locals.put(locals,'error','email_invalid'));
		if(req.body.email == req.session.email)
			return res.render('change_email', res.locals.put(locals,'error','email_unchanged'));

		db.select('users', ['email'], ['email'], [req.body.email], null, function(err, results){
			if(err)
				return res.render('change_email', res.locals.put(locals, 'error', 'server'));

			if(results && results.rows && results.rows[0])
				return res.render('change_email', res.locals.put(locals, 'error', 'email_exists_change'));

			var code = createKey();
			db.upsert('email_change', ['email'], ['email', 'new_email', 'code', 'expires'], [req.session.email, req.body.email, code, Date.now() + (1000 * 60 * 60 * 24)], [null, null, null, 'bigint'], function(err, result){
				if(err)
					return res.render('change_email', res.locals.put(locals, 'error', 'server'));

				mail.sendCode(req.body.email, code, 'change-email', function(err, data){
					if(err)
						logger.error('Error occurred while sending email change code. '+err+'. '+req.body.email+'=>'+code);
				});

				locals.email_domain = locals.email.split('@')[1];
				return res.render('change_email', res.locals.put(locals, 'success', true));
			});
		});
	});

	app.get('/user/verify-email-change', function(req, res){
		var locals = {title: 'Verify Account Email Change', resend_url: '/user/change-email'};
		if(!req.query.code)
			return res.render('verify', res.locals.put(locals,'error','code_required'));

		db.select('email_change', ['*'], ['code'], [req.query.code], null, function(err, results){
			if(err || !results.rows || !results.rows[0])
				return res.render('verify', res.locals.put(locals,'error','code_invalid'));

			destroyKey('email_change', results.rows[0].email);

			if(Date.now() > results.rows[0].expires)
				return res.render('verify', res.locals.put(locals,'error','code_expired'));

			db.update('users', ['email'], [results.rows[0].new_email], ['email'], [results.rows[0].email], function(err, results){
				if(err || !results.rows || !results.rows[0])
					return res.render('verify', res.locals.put(locals,'error','server'));
				
				locals.success = true;
				req.session.email = results.rows[0].email;
				res.render('verify', res.locals.put(locals,'success',true));
			});
		});
	});

	// User account actions
	app.get('/user/regenerate-keys', checkAccess, function(req, res){
		res.render('regenerate_keys', {title: 'Regenerate API Key and Secret'});
	});

	app.post('/user/regenerate-keys', checkAccess, function(req, res){
		var locals = {title: 'Regenerate API Key and Secret'};

		if(!req.body.password)
			return res.render('regenerate_keys', res.locals.put(locals,'error','pass_required'));

		db.select('users', ['pass_hash'], ['id'], [req.session.uid], null, function(err, results){
			if(err)
				return res.render('regenerate_keys', res.locals.put(locals,'error','server'));

			bcrypt.compare(req.body.password, results.rows[0].pass_hash, function(err, valid){
				if(err || !valid)
					return res.render('regenerate_keys', res.locals.put(locals,'error','bad_password'));

				var key = createKey(), secret = createKey();
				db.update('users', ['api_key', 'api_secret', 'updated_at'], [key, secret, Date.now()], ['id'], [req.session.uid], function(err, result){
					if(err || !result.rows || !result.rows[0])
						return res.render('regenerate_keys', res.locals.put(locals,'error','server'));

					res.redirect(302, '/user/dashboard');
				});
			});
		});

	});

};
