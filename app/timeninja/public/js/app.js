;
$(window).ready(function(){
	var app = window.app = {};

	app.errors = {
		'email_required': 'Email address is definitely required.',
		'email_invalid': 'This email address does not appear to be valid.',
		'email_exists': 'This email has already been registered. <a href="/login?e={{email}}">Login now?</a>',
		'no_such_email': 'This email has not been registered. <a href="/signup?e={{email}}">Sign up now?</a>',
		'pass_required': 'Password is definitely required.',
		'bad_password': 'The password you entered was incorrect. <a href="/reset-password?e={{email}}">Forgot your password?</a>',
		'not_verified': 'This email address has not been verified yet. <a href="/resend-verification?e={{email}}">Resend verification email?</a>',
		'password_not_set': 'This account does not have a password yet. You will have to <a href="/reset-password?e={{email}}">reset your password</a> before logging in.',
		'server': 'Something went wrong. This is definitely our mistake. Try again, and if you\'re still having problems <a href="mailto:support@timeninja.net">contact support</a>.',
		'password_mismatch': 'The passwords you entered were different. Both passwords should be same.',
		'email_verified': 'This email address has already been verified. <a href="/login?e={{email}}">Login now?</a>',
		'email_unchanged': 'The email address you provided is the same as your currently registered email address.',
		'email_exists_change': 'This email address has already been registered for another account. If this email belongs to you <a href="mailto:support@timeninja.net">contact support</a>.'
	};

	app.validators = {
		'email': [{ pattern: /.+/g, msg: 'email_required' },
				{ pattern: /[\w\.+]+@\w+\.[\w\.]+/g, msg: 'email_invalid' }],
		'password': [{ pattern: /.+/g, msg: 'pass_required' }]
	};

	app.showValidationError = function(selector, error, replacers, changeClass){
		changeClass = changeClass === false ? false : true;
		var title = app.errors[error] || '';
		for(var r in replacers)
			title = title.replace('{{'+r+'}}',replacers[r]);
		title = title.replace(/{{[^}}]+}}/g, '');
		$(selector)
			.tooltip('destroy')
			.addClass(changeClass ? 'error' : '')
			.tooltip({placement: 'top', title: title, html: true, trigger: 'manual'})
			.tooltip('show')
			.find('input')
				.on('keypress', function(){
					$(selector).tooltip('hide').removeClass('error');
				})
				.focus();
	};

	app.validate = function(selector, rule){
		if(!app.validators[rule])
			return false;
		var $e = $(selector);
		var $i = $e.find('input');
		var text = $i.val();
	
		for(var r in app.validators[rule]){
			var matches = text.match(app.validators[rule][r].pattern);
			if(!(matches && matches[0] && matches[0] == text)){
				app.showValidationError($e, app.validators[rule][r].msg);
				return false;
			}
			else{
				$eq = $($e.attr('data-validate-equals')).first();
				if($eq.length && $eq.val() != text){
					app.showValidationError($eq.parents('.control-group'), 'password_mismatch');
					$e.addClass('error').find('input').on('keypress', function(){
						$(selector).tooltip('hide').removeClass('error');
					});
					return false;
				}
			}
		};

		if($i.attr('data-mustchange') == 'true' && text == $i.attr('data-original-value')){
			app.showValidationError($e, $i.attr('data-nochange-error'));
			return false;
		}

		return true;
	};

	app.formValidator = function(){
		var $cgs = $(this).find('.control-group');
		for(var cg=0;cg<$cgs.length;++cg){
			var rule = $($cgs[cg]).attr('data-validate');
			if(rule && !app.validate($cgs[cg], rule))
				return false;
		}

		if($(this).attr('data-confirm') == 'true'){
			app.confirmRegenerate();
			return false;
		}
		return true;
	};

	app.setupValidation = function(){
		$('form').not('[data-ajax="true"]').on('submit', app.formValidator);
	};
	app.setupValidation();

	app.setFocus = function(){
		var i = 0;
		var $fe = $(document.activeElement);
		if(!$fe.is('input')){
			do
				var $e = $('input[data-focus="'+(++i)+'"]').first();
			while($e.length > 0 && $e.val().length > 0)
			$e = $e.length ? $e : $('input[data-focus="'+(--i)+'"]').first();
			$e.focus();
		}
	};
	app.setFocus();

	app.setupNoSubmit = function(){
		$('form[data-nosubmit="true"]').on('submit', function(){ return false; });
	};
	app.setupNoSubmit();

	app.confirmRegenerate = function(fn){
		$('#modal-confirm').modal({keyboard: false});
		$('#modal-confirm #btn-yes').off('click').on('click', function(){
			$('#modal-confirm').modal('hide');
			$('form').off('submit').trigger('submit');
		});
		$('#modal-confirm #btn-no').off('click').on('click', function(){
			$('#modal-confirm').modal('hide');
			window.location.href = '/user/dashboard';
		});
	};

	/*
	// Dashboard specific actions	
	var dash = window.dash = {};

	dash.setupButtonActions = function(){
		$('button[data-action="edit"]').off('click').on('click', function(){
			$(this).attr('data-action', 'submit').text('Submit').prop('disabled', false).siblings('input').prop('disabled', false).focus();
			dash.setupButtonActions();
		});
		$('button[data-action="submit"]').off('click').on('click', function(){
			var $form = $(this).attr('data-action', 'edit').text('Updating').prop('disabled', true).siblings('input').prop('disabled', true).parents('.control-group');
			dash.setupButtonActions($form);
			dash.submitForm($(this).parents('.control-group'));
		});
	};
	dash.setupButtonActions();

	dash.resetForm = function(selector){
		selector = $(selector);
		var $i = selector.find('input');
		$i.val($i.attr('data-original-value')).prop('disabled', true);

		var $b = $(selector).find('button');
		$b.text($b.attr('data-caption')).prop('disabled', false);
	};

	dash.disableModalClose = function(){
		$('#modal-confirm').find('.close').prop('disabled', true);
		$('.modal-backdrop').off('click');
	};

	dash.enableModalClose = function(){
		$('#modal-confirm').find('.close').prop('disabled', false);
		$('.modal-backdrop').on('click', function(){
			$('#modal-confirm').modal('hide');
		});
	};

	dash.confirm = function(type, fn){
		var $m = $('#modal-confirm');
		$m.find('.modal-body > div:not(.'+type+')').css('display', 'none');
		$m.find('.modal-body > div.'+type+'').css('display', 'block');
		$m.attr('data-type',type);
		$m.find('input').val('');
		$m.find('.control-group').tooltip('destroy').removeClass('error');

		$m.find('form').on('submit', function(){
			if(app.formValidator.call(this))
				fn(false, {password: $(this).find('input').val()});
		});

		$m.on('hidden', function(){
			fn(true);
		}).modal({keyboard: false});
		setTimeout(function(){$m.find('input[data-focus="1"]').focus();}, 200);
	};

	dash.toggleSpinner = function(){
		var $m = $('#modal-confirm');
		var $s = $m.find('.modal-body > div.spinner');
		var sv = $s.is(':visible');

		$m.find('.modal-body > div.'+$m.attr('data-type'))
			.css('display', sv ? 'block' : 'none');
		$s.css('display', sv ? 'none' : 'block');
	};

	dash.showModalError = function(error){
		var $m = $('#modal-confirm');
		var $e = $('.modal-body > div.error-msg');

		$m.find('.modal-body > div:not(.error-msg)').css('display', 'none');
		$e.css('display', 'block').html(app.errors[error]);
	};

	dash.submitForm = function(selector){
		selector = $(selector);
		if(app.validate(selector, selector.attr('data-validate'))){
			dash.confirm(selector.attr('data-confirm'), function(err, data){
				if(err)
					return dash.resetForm(selector);
				dash.disableModalClose();
				dash.toggleSpinner();
				var data = { _csrf: $('#_csrf').val(), password: data.password };
				data[selector.attr('data-key')] = selector.find('input').val();
				$.ajax({
					url: selector.attr('data-endpoint'),
					type: 'POST',
					data: data,
					dataType: 'json',
					success: function(){
						selector.find('button').text('Edit').prop('disabled', false);
						setTimeout(function(){ selector.tooltip('destroy'); }, 3000);
					},
					error: function(xhr){
						var showFormError = function(selector, error, changeClass, modal){
							if(modal)
								dash.showModalError(error);
							else{
								$('#modal-confirm').modal('hide');
								app.showValidationError(selector, error, null, changeClass);
							}
						};

						dash.toggleSpinner();
						dash.enableModalClose();
						if(xhr.status == 400){							
							var data = JSON.parse(xhr.responseText);

							if(error.indexOf('password') > -1)
								app.showValidationError($('#modal-confirm > div.password .control-group'), data.error);
							else
								showFormError(selector, data.error);
						}
						else
							showFormError($('#modal-confirm > div.password .control-group'), 'server', false, true);
					}
				});
			});
		}
		else
			selector.find('button').trigger('click');
	};
	*/
});