
/*
 * Application Configuration
 */

var fs = require('fs');
var extend = require('../utils/extend');

// Dev config
var env = process.env.NODE_ENV || 'development';

// Common config settings
var appRoot = fs.realpathSync(__dirname+'/../');
var config = {
	logger: {
		file: appRoot+'/logs/run.log',
		level: 'info'
	},
	nginx: {
		basedir: appRoot+'/public',
		configFile: appRoot+'/config/nginx.conf'
	},
	minify: {
		js: { merge: [], split: [] },
		css: { merge: [], split: [] },
	},
	session: {
		key: 'PHPSESSID',
	},
	db: {
		name: 'timedb',
		host: 'localhost'
	},
	plans: [null,
		{name: 'Starter', requests: {max: 5000}},
		{name: 'Regular', requests: {max: 10000}},
		{name: 'Premier', requests: {max: 50000}}]
};

if('development' == env){
	var env_config = {
		session: {
			secret: 'bfd49e29abedb1c4e391e201770a4033770485773afb265b50b05fb01ceffa14'
		},
		db: {
			user: 'timeapi_dev',
			pass: 'qwaszxwesdxc',
		}
	};
}
else if('production' == env){
	var env_config = {
		minify: {
			js: { merge: ['jquery.1.10.2','jquery.scrollto.1.4.6','bootstrap','app'], split: ['gauge'] },
			css: { merge: ['bootstrap', 'bootstrap-responsive', 'app'], split: [] },
		},
		session: {
			secret: 'be2761c12a5d5c596ef7aa9df63cd012a964184d4ffecc2178cb204e14090a03'
		},
		db: {
			user: 'timeapi_prod',
			pass: 'qwaszxwesdxc',
		}
	};
}

module.exports = extend(true, config, env_config);